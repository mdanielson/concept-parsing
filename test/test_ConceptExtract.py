
from concepts import defaults, MultiWordConcepts

def test_extraction():
    nlp = defaults.make_default_pipeline("en_core_web_sm")

    message = nlp("I am going to the market to buy vegetables and some fruits.")
    for i in message:
        print(i, i.pos_, i.is_stop)

    print(MultiWordConcepts.valid_noun_chunks(nlp("some fruits")))

    answer = MultiWordConcepts.extract_bag_of_concepts_as_strings(message)
    print(answer)
    assert answer == {'buy_vegetable', 'go_market', 'vegetable', 'fruit', 'buy_fruit', 'market'}

    graph = MultiWordConcepts.make_concept_graph(message)
    assert "Message" in graph.nodes()
    for node in graph.nodes():
        print(node)
    assert graph.number_of_nodes() == 7
    print(list(graph.edges()))
    assert graph.number_of_edges() == 6

    message = nlp("I am going on a walk in the park")
    result = MultiWordConcepts.extract_bag_of_concepts_as_strings(message)
    print(result)
    assert result == {'go_park', 'go_walk', 'walk', 'park'}
