import spacy
from concepts import DependencyConceptExtraction, common, defaults

def to_strings(items):
    nset = set()
    for item in items:
        nset.add(tuple(str(c) for c in item.tokens))
    return nset

nlp = defaults.make_default_pipeline()

def run_check(prefix, ans, real):
    for i in range(len(ans)):
        print(prefix, ans[i])
        print(prefix, real[i])
        assert ans[i].kind == real[i][0]
        assert toks_to_str(ans[i].tokens) == real[i][1]


def test_single_word():
    m = nlp("Erik threw a birthday party for his girlfriend")
    for item in m:
        print(item, item.pos_, item.dep_)
    ans = DependencyConceptExtraction.get_single_word(m, ())
    assert to_strings(ans) == set((("threw",), ("Erik",), ("girlfriend",), ('his',))), to_strings(ans)


def test_complex_nouns():

    m = nlp("Erik threw a birth day party for his girlfriend")
    ans = DependencyConceptExtraction.complex_nouns(m[5])
    print(ans)
    assert to_strings(ans) == set([("birth", "day", "party")])

    m = nlp("Erik threw a birthday party for his girlfriend")
    ans = DependencyConceptExtraction.complex_nouns(m[4])
    print(ans)
    assert to_strings(ans) == set([("birthday", "party")])

def test_adverbial_clause_modifier():
    m = nlp("The machine slows down when the best games are playing")
    ans = DependencyConceptExtraction.adverbial_clause_modifier(m[9])

    assert to_strings(ans) == set([("playing", "slows")])
    for c in ans:
        for item in c.tokens:
            print(item, item.lemma_)

def test_prepositional_phrases():
    m = nlp("Bob hit Marie with a hammer")

    ans = DependencyConceptExtraction.prepositional_phrases(m[3])

    assert to_strings(ans) == set([("hit", "with"), ("hit", "with", "hammer")])


def test_modifiers():
    m = nlp("Paul is a bad loser")
    ans = DependencyConceptExtraction.modifiers(m[4])
    assert to_strings(ans) == set([("bad", "loser")])


def test_open_clausal_complements():
    m = nlp("Paul likes to praise good movies.")
    ans = DependencyConceptExtraction.open_clausal_complements(m[1])
    assert to_strings(ans) == set([("likes", "praise", "movies")])



def test_negation():

    m = nlp("I did not like the movie")
    ans = DependencyConceptExtraction.negation(m[3])
    assert to_strings(ans) == set([("not", "like")])
    m = nlp("I did really not like the movie")

    ans = DependencyConceptExtraction.negation(m[4])
    m = nlp("I did not really love the movie")
    ans = DependencyConceptExtraction.negation(m[4])
    assert to_strings(ans) == set([("not", "love")])

    m = nlp("Bill is not a scientist")
    ans = DependencyConceptExtraction.negation(m[1])
    assert to_strings(ans) == set([("not", "scientist")])

    m = nlp("Well i'm not sure about the story and it did seem biased.")
    ans = sorted(DependencyConceptExtraction.concepts_for_sentence(m))
    for i in ans:
        print("Negation:", i)
    assert len(ans) == 12
    real = [
        ('AdjectivalComplement', "be_sure"),
        ('MultiWord', "be_story"),
        ('MultiWord', "story"),
        ('Negation', "not_sure"),
        ('NounSubjectAdjectiveComplement', "-PRON-_sure"),
        ('ObjectDep', "seem_biased"),
        ('PrepositionalPhrase', "sure_about"),
        ('PrepositionalPhrase', "sure_about_story"),
        ('SingleWord', "not"),
        ('SingleWord', "sure"),
        ('SingleWord', "seem"),
        ('SingleWord', "biased"),
    ]
    run_check("test_negation", ans, real)

def test_adjective_and_clausal_complements():
    m = nlp("This meal smells bad.")
    ans = DependencyConceptExtraction.adjective_and_clausal_complements(m[2])

    assert to_strings(ans) == set([("smells", "bad")])

def test_direct_nominal_objects():
    m = nlp("Paul saw the movie in 3D")
    ans = DependencyConceptExtraction.direct_nominal_objects(m[1])
    assert to_strings(ans) == set([("saw", "movie")])

def test_nsubject_acomp_rule():
    m = nlp("The flower smells bad")
    ans = to_strings(DependencyConceptExtraction.nsubj_acomp_rule(m[1]))
    assert ans == set([('flower', 'bad')])


def toks_to_str(toks):
    return common.join_tuple(c.lemma_ for c in toks)

def test_them_all():
    m = nlp("the flower smells bad")
    ans = sorted(DependencyConceptExtraction.concepts_for_sentence(m))
    print(ans)
    assert toks_to_str(ans[0].tokens) == "smell_bad"
    assert ans[0].kind == "AdjectivalComplement"
    assert toks_to_str(ans[1].tokens) == "flower"
    assert ans[1].kind == "MultiWord"
    assert toks_to_str(ans[2].tokens) == "smell_flower"
    assert ans[2].kind == "MultiWord"
    assert toks_to_str(ans[3].tokens) == "flower_bad"
    assert ans[3].kind == "NounSubjectAdjectiveComplement"
    assert toks_to_str(ans[4].tokens) == "bad"
    assert ans[4].kind == "SingleWord"


    m = nlp("Erik threw a birthday party for his girlfriend.")
    print(m)
    ans = sorted(DependencyConceptExtraction.concepts_for_sentence(m))
    assert len(ans) == 12, ans
    for i in ans:
        print("THROW:", i)
    assert toks_to_str(ans[0].tokens) == "birthday_party"
    assert ans[0].kind == "ComplexNoun"
    assert toks_to_str(ans[1].tokens) == "erik"
    assert ans[1].kind == "MultiWord"
    assert toks_to_str(ans[2].tokens) == "throw_erik"
    assert ans[2].kind == "MultiWord"
    assert toks_to_str(ans[3].tokens) == "throw_birthday"
    assert ans[3].kind == "MultiWord"
    assert toks_to_str(ans[4].tokens) == "throw_birthday_party"
    assert ans[4].kind == "MultiWord"
    assert toks_to_str(ans[5].tokens) == "throw_-PRON-_girlfriend"
    assert ans[5].kind == "MultiWord"
    assert toks_to_str(ans[6].tokens) == "birthday"
    assert ans[6].kind == "MultiWord"
    assert toks_to_str(ans[7].tokens) == "birthday_party"
    assert ans[7].kind == "MultiWord"
    assert toks_to_str(ans[8].tokens) == "-PRON-_girlfriend"
    assert ans[8].kind == "MultiWord"
    assert toks_to_str(ans[9].tokens) == "throw_birthday_party"
    assert ans[9].kind == "ObjectDep"
    assert toks_to_str(ans[10].tokens) == "throw_for"
    assert ans[10].kind == "PrepositionalPhrase"
    assert toks_to_str(ans[11].tokens) == "throw_for_girlfriend"
    assert ans[11].kind == "PrepositionalPhrase"


    print(ans)
    m = nlp("I am going to the market to buy some vegetables and some fresh fruits")
    print(m)
    ans = sorted(DependencyConceptExtraction.concepts_for_sentence(m))
    for i in ans:
        print("MARKET", i)
    assert len(ans) == 14
    real = [
        ("AdverbialClauseModifier", "buy_go"),
        ("Modifier", "fresh_fruit"),
        ("MultiWord", "go_market"),
        ("MultiWord", "market"),
        ("MultiWord", "buy_vegetable"),
        ("MultiWord", "buy_fresh"),
        ("MultiWord", "buy_fresh_fruit"),
        ("MultiWord", "vegetable"),
        ("MultiWord", "fresh"),
        ("MultiWord", "fresh_fruit"),
        ("ObjectDep", "buy_vegetable"),
        ("ObjectDep", "buy_fruit"),
        ("PrepositionalPhrase", "go_to"),
        ("PrepositionalPhrase", "go_to_market"),
    ]
    run_check("Market", ans, real)
    m = nlp("I like the coffee at Starbucks in St. Andrews")
    ans = sorted(DependencyConceptExtraction.concepts_for_sentence(m))
    real = [
        ("ComplexNoun", "st._andrews"),
        ('MultiWord', "like_coffee"),
        ('MultiWord', "like_starbucks"),
        ('MultiWord', "like_st._andrews"),
        ('MultiWord', "coffee"),
        ('MultiWord', "starbucks"),
        ('MultiWord', "st._andrews"),
        ("ObjectDep", "like_coffee"),
        ('PrepositionalPhrase', "coffee_at"),
        ('PrepositionalPhrase', "coffee_at_starbucks"),
        ('PrepositionalPhrase', "coffee_in"),
        ('PrepositionalPhrase', "coffee_in_st._andrews"),
    ]
    run_check("COFFEE", ans, real)



    m = nlp("I can even now remember the hour from which I dedicated myself to this great enterprise")
    ans = sorted(DependencyConceptExtraction.concepts_for_sentence(m))
    for i in ans:
        print(i)
    real = [
        ('Modifier', "even_now"),
        ('Modifier', "even_now_remember"),
        ('Modifier', "great_enterprise"),
        ('MultiWord', "remember_hour"),
        ('MultiWord', "hour"),
        ('MultiWord', "dedicate_great"),
        ('MultiWord', "dedicate_great_enterprise"),
        ('MultiWord', "great"),
        ('MultiWord', "great_enterprise"),
        ('ObjectDep', "remember_hour"),
        ('ObjectDep', "dedicate_-PRON-"),
        ('PrepositionalPhrase', "dedicate_from"),
        ('PrepositionalPhrase', "dedicate_from_which"),
        ('PrepositionalPhrase', "dedicate_to"),
        ('PrepositionalPhrase', "dedicate_to_enterprise"),
        ('SingleWord', "even"),
        ('SingleWord', "now"),
        ('SingleWord', "which"),
    ]
    run_check("GREAT", ans, real)
    assert len(ans) == 18
    assert len(common.extract_unique_concepts(ans)) == 16


    m = nlp("The food smells bad")
    ans = sorted(DependencyConceptExtraction.concepts_for_sentence(m))
    for i in ans:
        print(i)
    assert len(ans) == 5
    assert ans[0].kind == "AdjectivalComplement"
    assert toks_to_str(ans[0].tokens) == "smell_bad"
    assert ans[1].kind == "MultiWord"
    assert toks_to_str(ans[1].tokens) == "food"
    assert ans[2].kind == "MultiWord"
    assert toks_to_str(ans[2].tokens) == "smell_food"
    assert ans[3].kind == "NounSubjectAdjectiveComplement"
    assert toks_to_str(ans[3].tokens) == "food_bad"
    assert ans[4].kind == "SingleWord"
    assert toks_to_str(ans[4].tokens) == "bad"
    assert len(common.extract_unique_concepts(ans)) == 5


    m = nlp("Battery life of the new phone is not very good")
    ans = common.extract_unique_concepts(DependencyConceptExtraction.concepts_for_sentence(m))
    for i in ans:
        print(i)
    assert len(ans) == 15
    assert "battery_life_of_phone" in ans
    assert "battery_life" in ans
    assert "battery_life_of" in ans
    assert "not_good" in ans
