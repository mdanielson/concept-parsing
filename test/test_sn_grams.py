
from concepts import defaults, sn_grams, common


def test_sn_grams():
    nlp = defaults.make_default_pipeline(wordnet=False, vader=False)

    text = nlp("A member of the Society then inquired of the president whether Dr. Ferguson was not to be officially introduced.")
    sentence = list(text.sents)[0]
    grams = sn_grams.sn_grams(sentence.root, n_gram_length=2, include_stop=True)
    print(grams)
    grams = [common.concept_to_string(c) for c in grams]
    assert len(grams) == 18
    assert "member_a" in grams

    text1 = nlp("Eat with wooden spoon")
    sent1 = list(text1.sents)[0]
    text2 = nlp("Eat with metallic spoon")
    sent2 = list(text2.sents)[0]

    grams1 = sn_grams.sn_grams(sent1.root, n_gram_length=2, include_stop=True)
    grams2 = sn_grams.sn_grams(sent2.root, n_gram_length=2, include_stop=True)
    print(grams1)
    print(grams2)
    grams1 = [common.concept_to_string(c) for c in grams1]
    grams2 = [common.concept_to_string(c) for c in grams2]
    assert "eat_with" in grams1
    assert "eat_with" in grams2
    assert "with_spoon" in grams1
    assert "with_spoon" in grams2

    m = nlp("I can even now remember the hour from which I dedicated myself to this great enterprise")
    sent = list(m.sents)[0]
    grams = sn_grams.sn_grams(sent.root, n_gram_length=2, include_stop=True)
    assert len(grams) == 15

    for g in grams:
        print(g)
    grams = [common.concept_to_string(c) for c in grams]
    assert "remember_-PRON-" in grams
    assert "remember_can" in grams
    assert "remember_even" in grams
    assert "remember_now" in grams
    assert "remember_hour" in grams
    assert "hour_the" in grams
    assert "hour_dedicate" in grams
    assert "dedicate_from" in grams
    assert "from_which" in grams
    assert "dedicate_-PRON-" in grams
    assert "dedicate_-PRON-" in grams
    assert "dedicate_to" in grams
    assert "to_enterprise" in grams
    assert "enterprise_this" in grams
    assert "enterprise_great" in grams
