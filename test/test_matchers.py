import spacy

from concepts import Tokenizer, Matchers, defaults

def test_match():
    nlp = defaults.make_default_pipeline("en")
    text = "mo."
    doc = nlp(text)

    assert len(doc) == 1

    for item in doc:
        print(item)
