An implementation of concept parsing, based upon:
 * sn-grams:  Syntactic Dependency-Based N-grams: More Evidence of Usefulness in Classification, and Syntactic n-grams as machine learning features for natural language processing.
 * sentic.net concept parser: https://sentic.net/concept-parser.pdf / "Dependency-Based Semantic Parsing for Concept-Level Text Analysis"


Author: matt.danielson@gmail.com

Demo:
* Configure your python as in `setup_environment`
* `python3 -m concepts.DependencyConceptExtraction --prompt`
