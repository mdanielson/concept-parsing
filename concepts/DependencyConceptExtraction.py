import argparse
import collections
import sys


import spacy

from . import defaults, MultiWordConcepts, common

_DEBUG = False

# Notes:
# Spacy tags are based on the Universal Dependency Tags as well as these: http://www.mathcs.emory.edu/~choi/doc/cu-2012-choi.pdf
# The following are from "Dependency-Based Semantic Parsing for Concept-Level Text Analysis"
#
# Proposed Additions:
# multiple prepositions.  Coffee in starbucks in st. andrews -> Starbucks in st andrews

# From spacy utils
SUBJ_DEPS = {'agent', 'csubj', 'csubjpass', 'expl', 'nsubj', 'nsubjpass'}
OBJ_DEPS = {'attr', 'dobj', 'oprd'} # Don't take the 'dative dependencies"


def get_compound_noun(token):
    """
    Find the entire nounphrase associated with this head noun

    Parameters
    ==========
    token : spacy.Token
    """
    start, end = common.get_span_for_compound_noun(token)
    return tuple(token.doc[start:end+1],)

def flatten_concepts(concepts):
    """
    Parameters
    ==========

    concepts : set of tuples
    """
    single_words = set()
    for concept in concepts:
        if concept.kind == "MultiWord":
            for token in concept.tokens:
                single_words.add(token.lemma_)
    return single_words


def get_single_word(text, current_concepts):
    """
    Single Word Concepts Words having part-of-speech VERB, NOUN, ADJECTIVE
and ADVERB are also extracted from the text. Single word concepts which exist in the
multi-word-concepts are discarded as they carry redundant information. For example,
concept party that already appears in the concept birthday party so, we discard the
concept party.

    Parameters
    ==========
    text: spacy.Token or spacy.Span
    """

    nouns = frozenset(["NOUN", "PROPN", "NUM"])
    concepts = set()
    # Find set of single words to ignore
    single_words = flatten_concepts(current_concepts)
    for token in text:
        if _DEBUG:
            print(token, token.pos_, token.dep_)
        if (token.pos_ == "VERB" or token.pos_ in nouns or token.pos_ == "ADJ" or token.pos_ == "ADV") and (not token.is_stop):

            # Auxillary verbs won't be interesting
            if token.pos_ == "VERB" and token.dep_ == "aux":
                continue
            # only add if not seen as a multi-word
            if token.lemma_ in single_words:
                continue
            if token.dep_ == "compound" or len(complex_nouns(token)) > 0:
                continue

            concepts.add(common.Concept("SingleWord", (token,)))
    return concepts


def complex_nouns(token):
    """
    Noun Compound Modifier
Trigger: the rule is activated when it finds a noun composed with several nouns. A
noun compound modifier of an NP is any noun that serves to modify the head
noun.
Behavior: if a noun-word w is modified by another noun-word t then the complex
concept (t-h) is extracted.
Example: in (10), the complex concept (birthday,party) is extracted.
(10)
 Erik threw a birthday party for his girlfriend.

    """
    concepts = set()
    if token.pos_ == "NOUN" or token.pos_ == "PROPN":
        # Only fire on the head noun. The head of a complex noun is not another noun
        head = token.head
        if head.pos_ == "NOUN" or head.pos_ == "PROPN":
            return concepts
        keep = []

        def get_lefts(t):
            r = []
            for l in t.lefts:
                if l.pos_ in ("NOUN", "PROPN", "NUM"):
                    r.extend(l.lefts)
                    r.append(l)
            return r

        keep += get_lefts(token)
        keep.append(token)
        if len(keep) > 1:
            concepts.add(common.Concept("ComplexNoun", tuple(keep)))

    return concepts


def adverbial_clause_modifier(token):
    """
    Adverbial clause modifier This kind of dependency concerns full clauses that act
as modifiers of a verb. Standard examples involve temporal clauses and conditional
structures.
Trigger: the rule is activated when the active token is a verb modified by an adverbial
clause. The dependent is the head of the modifying clause.
Behavior: if a word t is a adverbial clause modifier of a word w then the concept (t-w)
is extracted.
Example: in (9), the complex concept (play,slow) is extracted.
(9)
 The machine slows down when the best games are playing.

    """
    concepts = set()
    if token.pos_ == "VERB" and token.dep_ == "advcl":
        head = token.head
        concepts.add(common.Concept("AdverbialClauseModifier", (token, head)))
    return concepts


def prepositional_phrases(token):
    """
    Prepositional phrases Although prepositional phrases do not always act as modifiers
we introduce them in this section as the distinction does not really matter for their
treatment.
Trigger: the rule is activated when the active token is recognized as typing a preposi-
tional dependency relation. In this case, the head of the relation is the element to
which the PP attaches, and the dependent is the head of the phrase embedded in the
PP.
Behavior: instead of looking for the complex concept formed by the head and depen-
dent of the relation, the system uses the preposition to build a ternary concept.
Example: in (8), the parser yields a dependency relation typed prep_with between
the verb hit and the noun hammer (=the head of the phrase embedded in the PP).
(8)
 Bob hit Marie with a hammer.
Therefore the system extracts the complex concept (hit, with, hammer).
    """
    concepts = set([])
    if token.dep_ == "prep":
        head = token.head
        if head.pos_== "NOUN" or head.pos_ == "NUM":
            head = get_compound_noun(head)
        else:
            head = (head,)
        for r in token.rights:
            if r.dep_ == "pobj":
                noun = get_compound_noun(r)
                concepts.add(common.Concept("PrepositionalPhrase", head + (token,) + noun, ))
                concepts.add(common.Concept("PrepositionalPhrase", head + (token,)))
    return concepts



def modifiers(token):
    """
    Adjectival, adverbial and participial modification The rules for items modified by
adjectives, adverbs or participles all share the same format.
Trigger: these rules are activated when the active token is modified by an adjective, an
adverb or a participle.
Behavior: if a word w is modified by a word t then the concept (t,w) is extracted.
Example: in (7) the concept bad, loser is extracted.
(7)
 a.
 Paul is a bad loser.

    """

    concepts = set()
    def collect_lefts(t):
        lefts = []
        for l in t.lefts:
            if l.dep_ == "amod" or l.dep_ == "advmod":
                lefts += collect_lefts(l)
                lefts.append(l)
        return lefts
    lefts = collect_lefts(token)
    if lefts and not (token.dep_ == "amod" or token.dep_ == "advmod"):
        # Long strings of modifiers may be intersting?
        if len(lefts) > 1:
            concepts.add(common.Concept("Modifier", tuple(lefts)))
        lefts.append(token)
        concepts.add(common.Concept("Modifier", tuple(lefts)))
    return concepts


def open_clausal_complements(token):
    """
    Open clausal complements Open clausal complements are clausal complements of a
verb that do not have their own subject, meaning that they (usually) share their subjects
with that of the matrix clause. The corresponding rule is complex in the same way as
the one for direct objects.
Trigger: when the active token is the head of the relation
Behavior: as for the case of direct objects, the algorithm tries to determine the structure
of the dependent of the head verb. Here the dependent is itself a verb, therefore, the
system tries to establish whether the dependent verb has a direct object or a clausal
complement of its own. In a nutshell, the system is dealing with three elements: the
head verb(h), the dependent verb(d), and the (optional) complement of the depen-
dent verb (t). Once these elements have all been identified, the concept (h,d,t) is
extracted
Example: in (6), like is the head of the open clausal complements dependency relation
with praise as the dependent and the complement of the dependent verb praise is
movie.
(6)
 Paul likes to praise good movies.
So, in this example the concept (like,praise,movie) is extracted.
    """
    concepts = set()
    for c in token.children:
        # open clausal complement
        if c.dep_ == "xcomp":
            for cc in c.children:
                if cc.dep_ in OBJ_DEPS:
                    cc = get_compound_noun(cc)
                    concepts.add(common.Concept("OpenClausalComplement", (token, c,) + cc))
    return concepts


def negation(token):
    """
    Negation Negation is also a crucial components of natural language text which usually
flips the meaning of the text. This rule is used to identify whether a word is negated in
the text.
Trigger: when in a text a word is negated.
Behavior: if a word h is negation by a negation marker t then the concept t-h is ex-
tracted.
Example: in (5), like is the head of the negation dependency relation with not as the
dependent. Here, like is negated by the negation marker not.
(5)
 I do not like the movie.
Based on the rule described above the concept (not, like) is extracted.
    """
    concepts = set()
    if token.pos_ == "VERB":
        for left in token.lefts:
            if left.dep_ == "neg":
                concepts.add(common.Concept("Negation", (left, token)))
        # iterates in sentence order
        ok_rights = set(["attr", "acomp"])
        neg = None
        for right in token.rights:
            if right.dep_ == "neg":
                neg = right
            elif right.dep_ in ok_rights and neg is not None:
                concepts.add(common.Concept("Negation", (neg, right)))
                break
    return concepts



def adjective_and_clausal_complements(token):
    """
    Adjective and clausal complements Rules These rules deal with verbs having as
complements either an adjective or a closed clause (i.e. a clause, usually finite, with its
own subject).
Trigger: when the active token is head verb of one of the complement relations.
Behavior: if a word h is in a direct nominal object relationship with a word t then the
concept h-t is extracted.
Example: in (4), smells is the head of a clausal complement dependency relation with
bad as the dependent.
(4)
 This meal smells bad.
In this example the concept (smell,bad) is extracted.

    """
    concepts = set([])
    for c in token.children:
        if c.dep_ == "acomp":
            concepts.add(common.Concept("AdjectivalComplement", (token, c)))
        elif c.dep_ == "ccomp":
            concepts.add(common.Concept("ClasualComplement", (token, c)))
    return concepts



def nsubj_acomp_rule(token):
    """
    Trigger: when the active token is found to be the syntactic subject of a verb and the
verb is on adjective complement relation with an adverb.
Behavior: if a word h is in a subject noun relationship with a word t and the word t
is with adjective complement relationship with a word w then the concept w-h is
extracted.
Example: In (2), flower is in a subject relation with smells and smells is in adjective
complement relationship with bad.
(2)
 The flower smells bad.
Here the concept (bad-flower) is extracted.
"""
    def children_any_acomp(token):
        ret = set()
        for t in token.children:
            if t.dep_ == "acomp":
                ret.add(t)
        return ret

    concepts = set()
    if token.dep_ == "nsubj":
        subj = get_compound_noun(token)
        # Check negation
        acomps = children_any_acomp(token.head)
        for c in acomps:
            concepts.add(common.Concept("NounSubjectAdjectiveComplement", subj + (c,)))
    return concepts


def direct_nominal_objects(token):
    """
    Direct nominal objects This complex rule deals with direct nominal objects of a verb.
Trigger: when the active token is head verb of a direct object dependency relation.
Behavior: if a word h is in a direct nominal object relationship with a word t then the
concept h-t is extracted.
Example: In (3) the system extracts the concept (see,movie).
(3)
 Paul saw the movie in 3D.
(see,in,3D) is not treated at this stage since it will later be treated by the standard
rule for prepositional attachment.

"""
    def traverse_conj(token):
        ret = set()
        for r in token.rights:
            if r.dep_ == "conj":
                ret.add(r)
                ret.update(traverse_conj(r))
        return ret


    concepts = set()
    if token.pos_ == "VERB":
        for c in token.children:
            if c.dep_ in OBJ_DEPS:
                dep = get_compound_noun(c)
                concepts.add(common.Concept("ObjectDep", (token,) + dep))
                for r in traverse_conj(c):
                    dep = get_compound_noun(r)
                    concepts.add(common.Concept("ObjectDep", (token,) + dep))

    return concepts


def lemmatize_concepts(concepts):
    new = set()
    for c in concepts:
        new.add(
            tuple(t.lemma_ for t in c.tokens)
        )
    return new


def concept_counts_for_sentence(sentence):
    concepts = MultiWordConcepts.concept_counts_for_sentence(sentence)

    for token in sentence:
        if token.is_space or token.is_punct:
            continue
        concepts.update(_unique_concepts_for_token(token))
    concepts.update(get_single_word(sentence, concepts))
    return concepts


def _unique_concepts_for_token(token):
    concepts = set()
    if token.pos_ == "PUNCT":
        return concepts
    concepts.update(direct_nominal_objects(token))
    concepts.update(nsubj_acomp_rule(token))
    concepts.update(adjective_and_clausal_complements(token))
    concepts.update(negation(token))
    concepts.update(open_clausal_complements(token))
    concepts.update(modifiers(token))
    concepts.update(prepositional_phrases(token))
    concepts.update(adverbial_clause_modifier(token))
    concepts.update(complex_nouns(token))
    return concepts


def concepts_for_sentence(sentence):
    # Ignore noun chunks that are attached to verbs in prepositional objects
    return set(concept_counts_for_sentence(sentence))


def concepts_for_text(text):
    concepts = []
    for sentence in text.sents:
        concepts.append(concepts_for_sentence(sentence))
    return concepts

def unique_concepts_for_text(text):
    concepts = set()
    for sentence in text.sents:
        concepts.update(common.concept_to_string(c) for c in concepts_for_sentence(sentence))
    return concepts


def do_one(text):
    for sentence in text.sents:
        print(sentence)
        for item in sorted(concepts_for_sentence(sentence)):
            print(item, common.concept_to_string(item))

class Extractor(object):
    def __call__(self, text):
        all_concepts = collections.Counter()
        for concepts in concepts_for_text(text):
            all_concepts.update(common.concept_to_string(c) for c in concepts)
        return all_concepts


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("--prompt", action="store_true")
    parser.add_argument("--parse", type=str)

    args = parser.parse_args()

    nlp = defaults.make_default_pipeline()
    if args.prompt:
        while True:
            text = input("Text:")
            do_one(nlp(text.strip("\n").lower()))
    elif args.parse:
        do_one(nlp(args.parse))

if __name__ == "__main__":
    main()
