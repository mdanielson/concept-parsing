import json
import collections
import re

import spacy.attrs
from spacy.tokenizer import Tokenizer
import spacy.matcher


MAGIC_CODES = {
    "ORTH": spacy.attrs.ORTH,
    "NORM": spacy.attrs.NORM,
}


def read_exceptions(filename):
    exceptions = {}
    with open(filename) as fd:
        for line in fd:
            try:
                blob = json.loads(line)
            except json.decoder.JSONDecodeError:
                print("An error ocurred during parsing")
                print(line)
                raise
            t = blob["token"]
            nparts = []
            # print(t)
            for part in blob["parts"]:
                npart = {}
                for k, v in part.items():
                    npart[MAGIC_CODES[k]] = v
                nparts.append(npart)
            exceptions[t] = nparts
    return exceptions


def create_custom_tokenizer(nlp, exceptions_file):
    for key, value in read_exceptions(exceptions_file).items():
        nlp.tokenizer.add_special_case(key, value)
    return nlp.tokenizer
