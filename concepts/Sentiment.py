
import spacy
from spacy.tokens import Doc
from nltk.sentiment.vader import SentimentIntensityAnalyzer


_vader_analyzer = SentimentIntensityAnalyzer()
def setup_vader():
    if not Doc.has_extension("vader_polarity_scores"):
        print ("Vader Sentiment setup")
        Doc.set_extension('vader_polarity_scores', getter=vader_polarity_scores)


def vader_polarity_scores(doc):
    return _vader_analyzer.polarity_scores(doc.text)
