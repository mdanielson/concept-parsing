import json
import os.path

from spacy.matcher import Matcher


def merge_and_flag(matcher, doc, i, matches):
    """
    Spacy does some jenky things with merging tokens.
    This function gets called for each match, but if you start merging early, the indices to merge will be off on subsequent calls.
    so you have to merge all matches during the final call of this function
    See: https://github.com/explosion/spaCy/issues/559

    Parameters
    ==========

    doc : spacy.Doc
    i : int
        The number of times this function has been called
    matches : list
        Metadata on each match
    """
    if i != len(matches)-1:
        return None
    # Get Span objects
    spans = [(ent_id, doc[start : end]) for ent_id, start, end in matches]
    for ent_id, span in spans:
        span.merge()


class GenericMatcher(object):
    """
    A Phrase Matcher that loads configuration from a file.  A Phrase Matcher combines sequences of tokens into one single token.
    The file should be one valid json blob per line.
    """
    def __init__(self, filename, nlp):
        self._matcher = Matcher(nlp.vocab)
        assert os.path.exists(filename), "Can't access {}".format(filename)

        with open(filename) as fd:
            for i, line in enumerate(fd):
                try:
                    pattern = json.loads(line)
                except json.JSONDecodeError:
                    print("An error ocurred during parsing")
                    print(line)
                    raise
                self._matcher.add("step_{}".format(i), [pattern], on_match=merge_and_flag)

    def __call__(self, doc):
        result = self._matcher(doc)
        return doc
