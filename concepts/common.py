import collections
import itertools


Concept = collections.namedtuple("Concept", ["kind", "tokens"])

def extract_unique_concepts(concepts):
    ans = set()
    for c in concepts:
        ans.add(concept_to_string(c))
    return ans

def join_tuple(tup, join_token=u"_"):
    ans = join_token.join(tup)
    return ans

def concept_to_string(c, join_token=u"_"):
    return join_tuple((t.lemma_ for t in c.tokens), join_token=join_token)

def concept_to_hashable(c, join_token=u"_"):
    return tuple([c.kind] + [t.lemma_ for t in c.tokens])


def concept_pos_to_string(c, join_token=u"_"):
    return join_tuple((t.pos_ for t in c.tokens), join_token=join_token)


def get_span_for_compound_noun(noun):
    """
    Find the indices of the longest noun phrase for the given head noun

    Parameters
    ==========
    noun : spacy.Token
            The head noun of a noun phrase

    Returns
    =======
    (int, int):  (start of span, end of span)
    """
    spans = get_tokens_for_compound(noun)
    idx = [n.i for n in spans]
    min_i, max_i = min(idx), max(idx)
    return (min_i, max_i)

def get_tokens_for_compound(noun):
    """
    Find the set of tokens for a given complex noun

    Parameters
    ==========
    noun : spacy.Token
            The head noun of a noun phrase

    Returns
    =======
    list
    """
    spans = []
    for l in noun.lefts:
        if l.pos_ in ("NOUN", "PROPN", "NUM"):
            spans.extend(get_tokens_for_compound(l))
    spans.append(noun)
    return spans


def is_english(rules, locale=None):
    if locale is not None and locale.lower() == "ar_qa":
        return False
    for rule in rules:
        if rule["ruleType"] == "STRING_EQUALS" and rule["ruleValue"].lower() == "es_us":
            return False
        if rule["ruleType"] == "STRING_NOT_EQUALS" and rule["ruleValue"].lower() == "en_us":
            return False
        if rule["ruleType"] == "STRING_EQUALS" and rule["ruleValue"].lower() == "hi_in":
            return False
        if rule["ruleType"] == "STRING_EQUALS" and rule["ruleValue"].lower() == "ar_Qa":
            return False
        if rule["ruleType"] == "STRING_EQUALS" and rule["ruleValue"].lower() == "ar":
            return False
    return True

