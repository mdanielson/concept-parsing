from senticnet import senticnet


class SenticNet(object):
    def __init__(self, language="en"):
        self._sn = senticnet.SenticNet(language=language)

    def concept(self, concept):
        """
        Catch Key Error
        """
        try:
            return self._sn.concept(concept)
        except KeyError:
            return None
