import pkg_resources

from . import Tokenizer, Matchers, Sentiment, SenticnetWrapper, WordNetPipeline

import spacy


_nlp = {}

def make_default_pipeline(lang="en_core_web_lg", wordnet=True, vader=True):
    """
    Create a default pipeline with all of the standard goodies included

    Parameters
    ==========

    lang : str
           Default is "en_core_web_lg"
    wordnet : Add the wordnet component?

    """
    global _nlp
    key = (lang, wordnet, vader)
    if key in _nlp:
        return _nlp[key]

    nlp = spacy.load(lang, disable=[])
    token_exceptions = pkg_resources.resource_filename('concepts', 'TokenExceptions.jsonish')
    generic_matches = pkg_resources.resource_filename('concepts', 'GenericMatches.jsonish')
    print("Using: {} and {}".format(token_exceptions, generic_matches))
    nlp.tokenizer = Tokenizer.create_custom_tokenizer(nlp, token_exceptions)
    # TODO: The syntax has changed apparently
    # nlp.add_pipe(Matchers.GenericMatcher(generic_matches, nlp), before="tagger")
    # if wordnet:
    #     nlp.add_pipe(WordNetPipeline.WordnetPipeline(nlp))
    # if vader:
    #     Sentiment.setup_vader()
    _nlp[key]= nlp
    return _nlp[key]


def make_simple_pipeline(lang="en_core_web_lg", disable=None):
    key = (None, None, None)
    if key in _nlp:
        return _nlp[key]
    else:
        nlp = spacy.load(lang, disable=disable if disable is not None else [])
    _nlp[key] = nlp
    return nlp
