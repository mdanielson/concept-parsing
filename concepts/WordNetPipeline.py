import argparse

import sys

from nltk.corpus import wordnet as wn
from spacy.tokens import Token


def penn_to_wn(tag):
    if tag.startswith('N'):
        return 'n'

    if tag.startswith('V'):
        return 'v'

    if tag.startswith('J'):
        return 'a'

    if tag.startswith('R'):
        return 'r'

    return None


class WordnetPipeline(object):
    def __init__(self, nlp):
        if not Token.has_extension("synsets"):
            Token.set_extension('synsets', default=None)

    def __call__(self, doc):
        for token in doc:
            ss = lookup_for_token(token)
            if ss:
                token._.set('synsets', ss)
            else:
                token._.set('synsets', [])

        return doc

def lookup_for_token(token):
    """
    Parameters
    ==========
    token : spacy.Token
    """
    wn_tag = penn_to_wn(token.tag_)
    return wn.synsets(token.text, wn_tag)



def lookup_for_str(token):
    """
    Parameters
    ==========
    token: str
    """
    return wn.synsets(token)


def hypernym_chain(synset):
    """
    Build the list of hypernyms (objects less specific than this object)
    Parameters
    ==========
    synset: wn.Synset

    Returns
    =======
    list
    """
    answer = [str(synset)]
    for item in synset.hypernyms():
        answer.extend(hypernym_chain(item))
    return answer


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("--lookup")
    args = parser.parse_args()

    if args.lookup:
        ss = lookup_for_str(args.lookup)
        for synset in ss:
            print("{}".format(synset))
            print("definition: {}".format(synset.definition()))
            print("examples: {}".format(synset.examples()))
            print("similar_to: {}".format(synset.similar_tos()))
            print("-"*10)


if __name__ == "__main__":
    main()
