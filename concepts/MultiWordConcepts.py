import collections
import re
import sys

import matplotlib.pyplot as plt
import networkx as nx
import nltk
import spacy


from . import defaults, common


Concept = common.Concept


nouns = frozenset(["NOUN", "PROPN", "NUM"])
adj = frozenset(["ADJ"])
# adj = frozenset(["ADJ", "ADP", "ADV"])

def valid_noun_chunks(noun_phrase):
    """
    Return bag of bigrams from noun chunks

    http://sentic.net/concept-parser.pdf

    and springerlink: sentic_computing. Section 3.1, Algorithm 1

    Ambiguity around Determiners

    Parameters
    ==========

    noun_phrase: spacy.Doc / spacy.Span

    Returns: Set of tuples of strings
    """
    concepts = set()
    noun_phrase = [token for token in noun_phrase if not token.is_space]
    if len(noun_phrase) == 1:
        word = noun_phrase[0]
        if not word.is_stop and word.pos_ in nouns:
            # A noun phrase of length one - should always be a noun
            concepts.add((noun_phrase[0],))
        return concepts

    for bigram in nltk.ngrams(noun_phrase, 2, pad_left=False, pad_right=False):
        first, second = bigram
        #print(first, first.is_stop, first.pos_, second, second.is_stop, second.pos_)
        if (first.pos_ in adj and second.pos_ in nouns):
            # Adjective Noun
            concepts.add((first, second))
        elif ((first.pos_ in nouns and first.is_stop) and second.pos_ in nouns):
            # None, Noun
            concepts.add((second,))
        elif (first.pos_ in nouns and second.pos_ in nouns):
            # Noun Noun
            concepts.add((first, second))
        elif first.pos_ in adj and second.is_stop:
            continue
        elif first.is_stop and second.pos_ in adj:
            continue
        else:
            # Add entire bigram
            def is_stop_or_det(t):
                return t.is_stop or t.pos_ == "DET"
            first_test = is_stop_or_det(first)
            second_test = is_stop_or_det(second)
            if first_test and not second_test:
                concepts.add((second, ))
            elif not first_test and second_test:
                concepts.add((first, ))
            elif not first_test and not second_test:
                concepts.add((first, second,))

    return concepts


def retrieve_verb_of_nc(noun):
    """
    Trace the head of this noun to find the verb it is attached to.

    Parameters
    ==========
    noun : spacey.Token
    """
    head = noun.head
    old = noun
    while head.pos_ != "VERB" and old != head:
        old = head
        head = head.head
    if head == old:
        return None
    return head

def concept_counts_for_sentence(sentence, ignore=None):
    """

    Parameters
    ==========
    sentence : spacy.Doc/Span
             : The sentence to exract concepts from
    ignore : collections.Counter
           : a dict of dep_ types to ignore
    """
    result = collections.Counter()
    for nc in sentence.noun_chunks:
        if ignore and nc.root.dep_ in ignore:
            continue
        # Find the head noun, and then the verb that it is attached to
        verb = retrieve_verb_of_nc(nc.root)
        for bigram in valid_noun_chunks(nc):
            result[Concept("MultiWord", bigram)] += 1
            if verb and not verb.is_stop:
                vt = (verb,)
                result[Concept("MultiWord", vt + bigram)] += 1
    return result


def concepts_for_sentence(sentence, ignore=None):
    """

    Parameters
    ==========
    sentence : spacy.Doc/Span
             : The sentence to exract concepts from
    ignore : set
           : a st of dep_ types to ignore
    """
    return set(concept_counts_for_sentence(sentence, ignore))

def concepts_for_sentence_by_verb_lemma(sentence):
    """
    """
    result = collections.defaultdict(set)

    for nc in sentence.noun_chunks:

        # Find the head noun, and then the verb that it is attached to
        verb = retrieve_verb_of_nc(nc.root)
        for bigram in valid_noun_chunks(nc):
            result[verb.lemma_].add(Concept("MultiWord", bigram))
    return result


def concepts_for_text(text):
    concepts = []
    for sentence in text.sents:
        concepts.append(concepts_for_sentence(sentence))
    return concepts


def extract_bag_of_concepts_as_strings(message):
    """
    Get the multi-word-concepts from the message
    """
    result = set()
    for sentence in message.sents:
        for c in concepts_for_sentence(sentence):
            result.add(common.join_tuple(t.lemma_ for t in c.tokens))
    return result


def make_concept_graph(message):
    """
    Construct a directed graph of the document.  First splits sentences, then extracts verb/noun concepts
    """
    g = nx.DiGraph()
    g.add_node("Message", label="Message", cls="MESSAGE")
    for i, sentence in enumerate(message.sents):
        sent_node = "sent_{}".format(i)
        g.add_node(sent_node, label=sent_node, cls="SENTENCE")
        g.add_edge("Message", sent_node)
        for verb, concepts in concepts_for_sentence_by_verb_lemma(sentence).items():
            vn_node = "{}_{}".format(sent_node, verb)
            g.add_node(vn_node, label=verb, cls="VERB")
            g.add_edge(sent_node, vn_node)
            for concept in concepts:
                concept_str = common.join_tuple((t.lemma_ for t in concept.tokens))
                # Matplotlib will try and interpolate $ symbols as fancy formatting
                concept_str = re.sub("\$", "\\$", concept_str)
                # graphviz may try and interpolate % symbols as fancy formatting
                concept_str = re.sub("%", "'%'", concept_str)
                g.add_node(str(concept_str), label=str(concept_str), cls="NOUN")
                g.add_edge(vn_node, str(concept_str))

    return g


def draw_graph(g, message="", figsize=(10, 10), graphviz_layout="neato", node_size=2000, font_size=18):
    f, ax = plt.subplots(figsize=figsize)
    plt.axis("off")

    layout = nx.drawing.nx_pydot.graphviz_layout(g, graphviz_layout)

    nx.draw_networkx_edges(g, pos=layout, ax=ax)
    messages = [node for node in g.nodes() if node.startswith("Message")]
    nx.draw_networkx_nodes(g, pos=layout, nodelist=messages, node_color="#fbb4ae", ax=ax, node_size=node_size)

    labels = nx.get_node_attributes(g, "label")
    nx.draw_networkx_labels(g, pos=layout, labels=labels, nodelist=messages, ax=ax, font_size=font_size)
    # Draw Sentence Nodes
    sents =[v for m in messages for v in g.neighbors(m)]
    nx.draw_networkx_nodes(g, pos=layout, nodelist=sents, node_color="#b3cde3", ax=ax, node_size=node_size)
    # Now drop dependent verb nodes
    verbs = [v for s in sents for v in g.neighbors(s)]
    nx.draw_networkx_nodes(g, pos=layout, nodelist=verbs, node_color="#ccebc5",ax=ax, node_size=node_size)
    # Finally draw noun concepts
    ncs = [nc for v in verbs for nc in g.neighbors(v)]
    nx.draw_networkx_nodes(g, pos=layout, nodelist=ncs, node_color="#decbe4",ax=ax, node_size=node_size)
    # Make sure text doesn't run off the end of the figure
    plt.tight_layout()
    return f


def main():
    nlp = defaults.make_default_pipeline("en")
    for i, line in enumerate(sys.stdin):
        message = nlp(line.strip("\n").lower())
        for token in message:
            print(token, token.norm_, token.pos_, token.is_stop)
        g = make_concept_graph(message)
        #print(g)
        fig = draw_graph(g, line)
        fig.savefig("fig_{}.png".format(i))
        print(extract_bag_of_concepts_as_strings(message))

if __name__ == "__main__":
    main()
