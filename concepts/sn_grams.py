import argparse
import collections
import itertools


from . import defaults, common


def sn_grams(token, n_gram_length=2, include_stop=True):
    """
    As defined in:
        Syntactic Dependency-Based N-grams: More Evidence of Usefulness in Classification
        and
        Grigori Sidorov, Francisco Velasquez, Efstathios Stamatatos, Alexander Gelbukh, and Lil-
        iana Chanona-Hernández. Syntactic n-grams as machine learning features for natural lan-
        guage processing. Expert Systems with Applications, 41(3):853–860, 2014.

    Parameters
    ==========
    token : spacy.Token
    n_gram_length : length of grams to generate
    include_stop : Boolean

    Returns
    =======
    list

    """

    if token.is_punct:
        return []

    def make_gram(n_gram_length, token, child, history):
        if n_gram_length == 2:
            return (token, child)
        elif n_gram_length == 3:
            if len(history) == 0:
                return
            else:
                return (history[-1], token, child)
        elif n_gram_length == 4:
            if len(history) < 2:
                return
            else:
                return (history[-2], history[-1], token, child)
        elif n_gram_length == 5:
            if len(history) < 3:
                return
            else:
                return (history[-3], history[-2], history[-1], token, child)

        assert False, (n_gram_length, token, child, history)

    def do_sn_grams(token, n_gram_length, include_stop, _history):
        grams = []

        for child in token.children:

            if child.is_punct:
                continue

            option = make_gram(n_gram_length, token, child, _history)

            if child.is_stop or token.is_stop:
                if include_stop:
                    if option is not None:
                        grams.append(option)
            else:
                if option is not None:
                    grams.append(option)

            grams.extend(
                do_sn_grams(
                    child,
                    n_gram_length=n_gram_length,
                    include_stop=include_stop,
                    _history=_history + [token]
                )
            )
        return grams
    grams = do_sn_grams(token, n_gram_length, include_stop=include_stop, _history=[])
    return [common.Concept(kind="sn-{}-grams".format(n_gram_length), tokens=g) for g in grams]


class Extractor(object):
    def __init__(self, length, to_string=True):
        self._length = length
        self._to_string = to_string

    def _do_one(self, text, all_concepts):
        for concept in sn_grams(text.root, n_gram_length=self._length):
            if self._to_string:
                concept = common.concept_to_string(concept)
            all_concepts[concept] += 1
        return all_concepts

    def __call__(self, text):

        all_concepts = collections.Counter()
        if hasattr(text, "sents"):
            for sentence in text.sents:
                self._do_one(sentence, all_concepts)
        else:
            return self._do_one(text, all_concepts)


class POSExtractor(object):

    def __init__(self, length):
        self._length = length

    def __call__(self, text):

        all_concepts = collections.Counter()
        for sentence in text.sents:
            for concept in sn_grams(sentence.root, n_gram_length=self._length):
                all_concepts[common.concept_pos_to_string(concept)] += 1

        return all_concepts


def concept_counts_for_sentence(text, length, to_string=False):
    bigrams = Extractor(length, to_string)
    return bigrams(text)



def concepts_for_text(text):
    concepts = []
    bigrams = Extractor(2)
    trigrams = Extractor(3)

    pos_bigrams = POSExtractor(2)
    pos_trigrams = POSExtractor(3)
    concepts.append(bigrams(text))
    concepts.append(trigrams(text))
    concepts.append(pos_bigrams(text))
    concepts.append(pos_trigrams(text))
    return concepts


def do_one(text):
    concepts = concepts_for_text(text)
    for items in concepts:
        print(items)


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument("--prompt", action="store_true")
    parser.add_argument("--parse", type=str)

    args = parser.parse_args()

    nlp = defaults.make_default_pipeline()
    if args.prompt:
        while True:
            text = input("Text:")
            do_one(nlp(text.strip("\n").lower()))
    elif args.parse:
        do_one(nlp(args.parse))

if __name__ == "__main__":
    main()
